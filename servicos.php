<?php
	//Template Name: Servicos
?>

<?php get_header(); ?>


<section class="sobre">
        <div class="sobre-container">
            <h2>Nossos serviços</h2>
            <p><?php the_field('paragrafo_servico');?></p>
            
            <div class="sobre-servicos">
                <div class="sobre-servicos-box">
                    <p><?php the_field('titulo_card_1');?></p>
                    <ul>
                        <li><?php the_field('topico_titulocard1_1');?></li>
                        <li><?php the_field('topico_titulocard1_2');?></li>
                        <li><?php the_field('topico_titulocard1_3');?></li>
                        <li><?php the_field('topico_titulocard1_4');?></li>
                        <li><?php the_field('topico_titulocard1_5');?></li>
                        <li><?php the_field('topico_titulocard1_6');?></li>
                        <li><?php the_field('topico_titulocard1_7');?></li>
                    </ul>
                </div>

                <div class="sobre-servicos-box">
                    <p><?php the_field('titulo_card_2');?></p>
                    <ul>
                        <li><?php the_field('topico_titulocard2_1');?></li>
                        <li><?php the_field('topico_titulocard2_2');?></li>
                        <li><?php the_field('topico_titulocard2_3');?></li>
                        <li><?php the_field('topico_titulocard2_4');?></li>
                        <li><?php the_field('topico_titulocard2_5');?></li>
                        <li><?php the_field('topico_titulocard2_6');?></li>
                    </ul>
                </div>
                
                <div class="sobre-servicos-box">
                    <p><?php the_field('titulo_card_3');?></p>
                    <ul>
                        <li><?php the_field('topico_titulocard3_1');?></li>
                        <li><?php the_field('topico_titulocard3_2');?></li>
                        <li><?php the_field('topico_titulocard3_3');?></li>
                        <li><?php the_field('topico_titulocard3_4');?></li>
                        <li><?php the_field('topico_titulocard3_5');?></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>	