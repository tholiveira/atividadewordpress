<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Aldrich&display=swap" rel="stylesheet">
    <title>DGR Contabilidade</title>
    <?php wp_head(); ?>
</head>
<body>

<header>
        <div class="container-header">
            <div class="logo">
                <a href="/DRG">DGR</a>
            </div>
            <div class="nav-bar-phone">
                <input type="checkbox" id="bt_menu">
                <label for="bt_menu" class="btn">&#9776;</label>
                <div class="nav-bar">
                    <nav>
                        <ul>
                            <li><a href="/DRG/servicos">Serviços</a></li>
                            <li><a href="#">Noticias</a></li>
                            <li><a href="/DRG/contato">Contato</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="phones">
                    <p>(21) 3619-3120</p>
                    <p>(21) 3619-5120</p>
                </div>
            </div>
        </div>
    </header>

