</body>
<footer>
    <div class="container-footer">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.5496582394303!2d-43.12337248520176!3d-22.89308858501879!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983c38b52bf21%3A0xdb63e2d4079246b2!2sR.+Maestro+Fel%C3%ADcio+Tol%C3%AAdo%2C+519+-+Centro%2C+Niter%C3%B3i+-+RJ%2C+24030-103!5e0!3m2!1spt-BR!2sbr!4v1564876185383!5m2!1spt-BR!2sbr" width="300" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div class="footer-contato">
            <p>Rua Maestro Felício Tolêdo, 519<br>Sala 502 - Centro de Niterói</p>
            <p>contato@dgrcontabilidade.com.br</p>
            <p>(21)3619-3120<br>(21)3619-5120</p>
        </div>
        <div class="footer-desenvolvido">
            <div class="footer-desenvolvido-container">
                <p>Desenvolvido por:</p>
                <img src="./assets/img/footer-logo.png">
            </div>
        </div>
    </div>
    <p class="footer-rodape">Copyright © 2019 - DGR Contabilidade todos os direitos reservados</p>
</footer>
<?php wp_footer(); ?>
</html>