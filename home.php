<?php
	//Template Name: Home
?>

<?php get_header(); ?>

<section class="main">
        <div class="blackout">
            <div class="container">
                <div class="logo-text">  
                    <img src=<?php the_field('logo_home')?> alt="logo">
                    <p><?php the_field('titulo_home');?></p>
                    <a href="DRG/contato">Entre em contato</a>
                </div>
            </div>
        </div>
    </section>

    <section class="values">
        <div class="container">
            <div class="values-cards">
                <div class="card">
                        <h2><?php the_field('titulo_card_1');?></h2>
                        <p><?php the_field('descricao_card_1');?></p>
                    </div>
                    <div class="card">
                        <h2><?php the_field('titulo_card_2');?></h2>
                        <p><?php the_field('descricao_card_2');?></p>
                    </div>
                    <div class="card">
                        <h2><?php the_field('titulo_card_3');?></h2>
                        <p><?php the_field('descricao_card_3');?></p>
                    </div>
            </div>
        </div>
        <div class="values-intro">
            <p><?php the_field('paragrafo_1');?></p>
            <p><?php the_field('paragrafo_1');?></p>
        </div>
    </section>

<?php get_footer(); ?>	