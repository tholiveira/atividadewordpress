<?php
	//Template Name: Contato
?>

<?php get_header();?>

<section class="contact-us">
    <div class="container">
        <div>
            <h1>Fale Conosco</h1>
        </div>
        <form action="action="<?php echo get_template_directory_uri();?>/enviar.php" method="post">
            <div class="group-itens">
                <label for="name">Nome:</label>
                <input type="text" id="name" name="name" placeholder="Ex:João da Silva">
            </div>
            <div class="group-itens">
                <label for="email">Email:</label>
                <input type="text" id="email" name="email" placeholder="Ex: joao-silva@exemplo.com.br">
            </div>
            <div class="group-itens">
                <label for="subject">Assunto:</label>
                <input type="text" id="subject" name="subject" placeholder="Ex: Abertura de Empresa">
            </div>
            <div class="group-itens">
                <label for="message">Mensagem:</label>
                <textarea name="message" id="message" placeholder="Digite sua mensagem aqui "></textarea>
            </div>
            <button class="sendBtn">Enviar</button>
        </form>
    </div>
</section>


<?php get_footer(); ?>	